! All lines starting with non-alphabetical or numerical character are comments
! 1) Settings that will be used in a main program.
Main:numberOfEvents = 10000          ! number of events to generate
Main:timesAllowErrors = 30          ! abort run after this many flawed events

! 2) Settings related to output in init(), next() and stat().
Init:showChangedSettings = on      ! list changed settings
Init:showAllSettings = off         ! list all settings
Init:showChangedParticleData = on  ! list changed particle data
Init:showAllParticleData = off     ! list all particle data
Next:numberCount = 1000            ! print message every n events
Next:numberShowLHA = 1             ! print LHA information n times
Next:numberShowInfo = 1            ! print event information n times
Next:numberShowProcess = 1         ! print process record n times
Next:numberShowEvent = 1           ! print event record n times
!Stat:showPartonLevel = on          ! additional statistics on MPI

! 3) Beam parameter settings. Values below agree with default ones.
Beams:idA = 2212                   ! first beam, p = 2212, pbar = -2212
Beams:idB = 2212                   ! second beam, p = 2212, pbar = -2212
Beams:eCM = 14000.                 ! CM energy of collision

! Make all hidden valley particles heavy so that they are not produced by mistake
4900001:m0 = 5000
4900002:m0 = 5000
4900003:m0 = 5000
4900004:m0 = 5000
4900005:m0 = 5000
4900006:m0 = 5000
4900011:m0 = 5000
4900012:m0 = 5000
4900013:m0 = 5000
4900014:m0 = 5000
4900015:m0 = 5000
4900016:m0 = 5000


4900023:m0 = 2000 ! Zd mass - variable
4900023:mWidth = 0.1 ! Zd width
HiddenValley:spinFV = 0
4900023:rescaleBR = 0.0001
4900023:12:bratio = 0.9999

HiddenValley:ffbar2Zv = on ! dark jet event processes

! Set the color gauge group of Hidden valley
! Color gauge group affects the hadronization
HiddenValley:Ngauge  = 2
! Number of flavours in the theory
HiddenValley:nFlav = 8
! the parameter used for the case of a running (first order) alpha_HV
HiddenValley:Lambda = 1.0  #lambda
! fixed alpha scale of gv/gammav emission; corresponds to alpha_strong of QCD
HiddenValley:alphaFSR = 1.0   #lambda
! Set spin of Hidden valley quarks (this should be set to 1, check whether it makes any difference)
HiddenValley:spinFv = 0
! switch on final-state shower of gv or gammav in a HV production process.
HiddenValley:FSR = on
! switch on string fragmentation of the HV partonic system. 
HiddenValley:fragment = on

! Set mass of HV quark, this should be consistent with MG5 settings
! Pythia should read off this parameter from LHE file, check if that works
! Pythia has only one HV quark at the moment
4900101:m0 = 1.0        # mass
! Set the width of the HV quark
4900101:mWidth = 0.2   # mass/100
! Given the mass and width, how far should pythia sample the Breight-Wigner distribution?
! Set corresponding mMin and mMax.
4900101:mMin = 0.8      # mass/2 - mass/100
4900101:mMax = 1.2      # mass/2 + mass/100

! a flavour-diagonal HV-meson with spin 0 that can decay back into the Standard-Model sector
4900111:m0 = 2     # mass
! a flavour-diagonal HV-meson with spin 1 that can decay back into the Standard-Model sector
4900113:m0 = 2    # mass
! an off-diagonal HV-meson with spin 0 that is stable and invisible
4900211:m0 = 0.99    # mass/2.0-0.01
! an off-diagonal HV-meson with spin 1 that is stable and invisible
4900213:m0 = 0.99      # mass/2.0-0.01

! lowest allowed pT of emission. Should be greater than or equal to 1.1 times Lambda, or it will be reset automatically.
HiddenValley:pTminFSR = 1.1    # 1.1*lambda
! fraction of HV-mesons that are assigned spin 1 (vector)
HiddenValley:probVector = 0.75

! Set the decay modes of the diagonal and offdigonal HV mesons
! They are to be set according to the formula given in comment
! Idealy speaking the inv factor is model dependent 
! Question: Does inv factor also depend on the Ngauge?
4900111:onechannel = 1  1.0  0 -3 3                         # 1 + to_st(1.0-inv) + 91 -3 3
!4900111:addchannel = 1  0.5  0 4900211 -4900211              # 1 + to_st(inv) + 0 4900211 -4900211
4900113:onechannel = 1  0.2 0 -1 1                         # 1 + to_st((1-inv)/5.) + 91 -1 1
4900113:addchannel = 1  0.2 0 -2 2                         # 1  + to_st((1-inv)/5.)  + 91 -2 2
4900113:addchannel = 1  0.2 0 -3 3                         # 1 + to_st((1-inv)/5.) + 91 -3 3
4900113:addchannel = 1  0.2 0 -4 4                         # 1 + to_st((1-inv)/5.) + 91 -4 4
4900113:addchannel = 1  0.2 0 -5 5                         # 1+ to_st((1-inv)/5.) +  91 -5 5
!4900113:addchannel = 1  0.5 0 4900213 -4900213              # 1 + to_st(inv) + 0 4900213 -4900213


! 6) Other settings. Can be expanded as desired.
! Note: may overwrite some of the values above, so watch out.
Tune:ee = 7
Tune:pp = 14                        ! use Tune 4Cx
ParticleDecays:limitTau0 = on      ! set long-lived particle stable ...
ParticleDecays:tau0Max = 10        ! ... if c*tau0 > 10 mm
