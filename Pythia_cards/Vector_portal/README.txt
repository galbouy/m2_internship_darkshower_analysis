Pythia cards produced with modified Simon Knapen's tool (arXiv:2103.01238)
(Python file used is in the directory)

Command and parameters :

tau = 0
Nf = 2
Nc = 2
mq=1
vector_portal.pythia_card("vector_tau0_%s_nflav_%s_ngauge_%s_mq_%s_stable.cmnd"%(str(int(tau)),str(int(Nf)),str(int(Nc)),str(int(mq))),
                         Nevent=50000,
                         m=2*mq,
                         xi_omega=0.8,
                         xi_Lambda=1,
                         ctau=tau,
                         Nc=Nc,
                         Nf=Nf,
                         mH=2000,
                         mayDecay=True,
                         userName="GAlbouy")


Set of parameters :

Nf = [2,8]
Nc = [2,3,5,8]
tau = [0]
m_q = [1,10]
