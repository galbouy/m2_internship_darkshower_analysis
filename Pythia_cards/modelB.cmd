! main00.cmnd
! This file contains commands to be read in for a Pythia8 run.
! Lines not beginning with a letter or digit are comments.
!

! 1) Settings used in the main program.
Main:numberOfEvents = 1000        ! number of events to generate
Main:timesAllowErrors = 3          ! how many aborts before run stops

! 2) Settings related to output in init(), next() and stat().
Init:showChangedSettings = on      ! list changed settings
Init:showChangedParticleData = on  ! list changed particle data
Next:numberCount = 100             ! print message every n events
Next:numberShowInfo = 2            ! print event information n times
Next:numberShowProcess = 2         ! print process record n times
Next:numberShowEvent = 2           ! print event record n times

! 3) Beam parameter settings. Values below agree with default ones.
Beams:idA = 2212                   ! first beam, p = 2212, pbar = -2212
Beams:idB = 2212                   ! second beam, p = 2212, pbar = -2212
Beams:eCM = 13000.                 ! CM energy of collision

! 4) Masses and couplings
! Masses
4900023:m0 = 2000 ! Zd mass - variable
4900023:mWidth = 0.1 ! Zd width
HiddenValley:spinFV = 0
4900023:rescaleBR = 0.0001
4900023:12:bratio = 0.9999
HiddenValley:Ngauge = 3 ! n dark QCD colors
 
4900101:m0 = 2.0 ! qd mass
4900111:m0 = 2.0 ! pi_d mass
4900113:m0 = 4.67 ! rho_d mass
4900211:m0 = 2.0 ! pi_d off-diag mass
4900213:m0 = 4.67 ! rho_d off-diag mass

4900111:addChannel = 1 1.0 102 4 -4
4900113:addChannel = 1 1.0 102 4900111 4900111
4900211:addChannel = 1 1.0 102 4 -4
4900213:addChannel = 1 1.0 102 4900111 4900111


! 5) Process selection
HiddenValley:ffbar2Zv = on ! dark jet event processes
HiddenValley:probVector = 0.173
HiddenValley:nFlav = 6
HiddenValley:fragment = on
HiddenValley:FSR = on
HiddenValley:alphaOrder = 1
HiddenValley:Lambda = 2.0
HiddenValley:pTminFSR = 16.5

! 6) Settings for the event generation process in the Pythia8 library.
! PartonLevel:MPI = off              ! no multiparton interactions
! PartonLevel:ISR = off              ! no initial-state radiation
! PartonLevel:FSR = off              ! no final-state radiation
! HadronLevel:Hadronize = off        ! no hadronization
