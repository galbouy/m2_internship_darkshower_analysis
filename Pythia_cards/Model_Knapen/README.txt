Pythia cards produced with modified Simon Knapen's tool (arXiv:2103.01238)
(Python file used is in the directory)

Command and parameters :

tau = 10
Nf = 2
Nc = 2
m_pi=2
Rinv=0
vector_portal.pythia_card("vector_tau0_%s_nflav_%s_ngauge_%s_rinv_%s.cmnd"%(str(int(tau)),str(int(Nf)),str(int(Nc)),str(int(Rinv))),
                         Nevent=10000,
                         m=m_pi,
                         xi_omega=1,
                         xi_Lambda=.5,
                         ctau=tau,
                         probVector=1-Rinv,
                         Nc=Nc,
                         Nf=Nf,
                         mH=2000,
                         mayDecay=True,
                         userName="GAlbouy")


Set of parameters :

Nf = [2,8]
Nc = [2,3,5,8]
tau = [10,30]
m_pi = [2,20]
Rinv = [0]