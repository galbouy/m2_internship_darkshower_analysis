- Vertex analysis for Delphes simulations files
- Need PyROOT analysis library and Delphes
- Necessary to be located in Delphes directory to run 

Vertex_origin.py :
	-> Comparison of pT in jets for different origin (Delphes reconstruction, ISR, dark vertices, truth particles)

	-> bash commmand : python Vertex_study_no_LRT.py delphes_file.root

Vertex_study_no_LRT.py :
	-> Study of vertices for Delphes file without LRT

	-> bash commmand : python Vertex_study_no_LRT.py delphes_file.root

Vertex_study_LRT.py :
	-> Study of vertices for Delphes file with LRT (add out of tracker vertices to jets)

	-> bash commmand : python Vertex_study_LRT.py delphes_file.root

Vertex_study_histlist.py : 
	-> Creates a list of histograms based on Vertex_study_LRT.py

	-> bash commmand : python Vertex_study_histlist.py delphes_file.root

Vertex_comparison.py :
	-> Comparison of two lists of histograms comming from Vertex_study_histlists.py

	-> bash commmand : python Vertex_comparison.py histlist1.root histlist2.root