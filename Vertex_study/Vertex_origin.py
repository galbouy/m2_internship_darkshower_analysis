# Comparison of pT in jets for different origin (Delphes reconstruction, ISR, dark vertices, truth particles)

# Input : delphes file
# Output : comparative plots of pT for jet 1/2

# bash commmand : python Vertex_origin.py delphes_file.root

# Author : G. Albouy, LPSC Grenoble, France

import sys
import numpy as np
import ROOT
from array import array
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

try:
  input = raw_input
except:
  pass

if len(sys.argv) < 2:
  print(" Usage: Vertex_origin.py delphes_file.root")
  sys.exit(1)

ROOT.gSystem.Load("libDelphes")

try:
  ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
  ROOT.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')
except:
  pass

inputFile = sys.argv[1]

print("Input Files :")
print(inputFile)

# Create chain of root trees
chain = ROOT.TChain("Delphes")
chain.Add(inputFile)

# Create object of class ExRootTreeReader
treeReader = ROOT.ExRootTreeReader(chain)
numberOfEntries = treeReader.GetEntries()

print("N events : %.0f"%numberOfEntries)

# Get pointers to branches used in this analysis
R = "14"       # Radius of jets reconstruction
branchJet = treeReader.UseBranch("ParticleFlowJet%s"%R)
branchParticle = treeReader.UseBranch("Particle")
branchEFlowTrack = treeReader.UseBranch("EFlowTrack")
branchEFlowPhoton = treeReader.UseBranch("EFlowPhoton")
branchEFlowNeutralHadron = treeReader.UseBranch("EFlowNeutralHadron")
branchTrack = treeReader.UseBranch("Track")
R=14.          # Set R to float to be used after

# Book histograms
PT_sup = 1200.
histPT1_jet = ROOT.TH1F("pt1_jet","pT from jet Jet 1;pT (GeV);Nevents",20,0.,PT_sup)
histPT2_jet = ROOT.TH1F("pt2_jet","pT from jet Jet 2;pT (GeV);Nevents",20,0.,PT_sup)
histPT1_vertex = ROOT.TH1F("pt1_vertex","pT from dark vertices Jet 1;pT (GeV);A.U.",20,0.,PT_sup)
histPT2_vertex = ROOT.TH1F("pt2_vertex","pT from dark vertices Jet 2;pT (GeV);Nevents",20,0.,PT_sup)
histPT1_part = ROOT.TH1F("pt1_part","pT from particles in Jet 1;pT (GeV);Nevents",20,0.,PT_sup)
histPT2_part = ROOT.TH1F("pt2_part","pT from particles in Jet 2;pT (GeV);Nevents",20,0.,PT_sup)
histPT1_ISR = ROOT.TH1F("pt1_isr","pT from ISR Jet 1;pT (GeV);Nevents",20,0.,PT_sup)
histPT2_ISR = ROOT.TH1F("pt2_isr","pT from ISR Jet 2;pT (GeV);Nevents",20,0.,PT_sup)


# Function to find original vertex from a final state particle
def Vertex_origin(i):
  M = i
  while 1:
    M1 = branchParticle.At(M).M1
    if branchParticle.At(M1).Status == 4:
      return "ISR"
    elif branchParticle.At(M1).PID == 4900113 or branchParticle.At(M1).PID == 4900111:
      return M1
    else:
      M=M1

            
# Loop over all events
Nevent = 10000
Nevent = numberOfEntries

pt_list_jet = array( 'f' , [])
pt_list_vertex = array( 'f' , [])
pt_list_ISR = array( 'f' , [])
pt_list_sum = array( 'f' , [])

for entry in range(0,Nevent):
  if entry%100==0: print(entry)

  # Read event
  treeReader.ReadEntry(entry)
  # Get number of particles and jets
  Npart = branchParticle.GetEntries()
  Njet = branchJet.GetEntries()
  Ntrack = branchTrack.GetEntries()
  # Set variables and lists
  Vertex1, Vertex2 = array( 'i' , []), array( 'i' , [])
  rho1, rho2 = array( 'i' , []), array( 'i' , [])
  pt1_vertex, pt2_vertex, pt1_part, pt2_part = 0,0,0,0
  Ncharge1 = 0
  pt1_ISR,pt2_ISR=0,0
  # 1st selection on Njet
  if Njet>1:
    jet1 = branchJet.At(0)
    jet2 = branchJet.At(1)
    # 2nd selection on pT and eta
    if jet1.PT>500 and np.abs(jet1.Eta)<2.5 and jet2.PT>0 and np.abs(jet2.Eta)<2.5:
      print("Event number : %i"%entry)
      #print("Number of particles : %i"%Npart)
      #print("Number of tracks : %i"%Ntrack)

      # Loop over all particles
      for i in range(0,Npart):
        part = branchParticle.At(i)
        if part.Status==1 and np.abs(part.PID)!=4900213 and np.abs(part.PID)!=4900211:
          # Compute dR between jet and part
          deltaR1 = np.sqrt((jet1.Phi-part.Phi)**2 + (jet1.Eta-part.Eta)**2)
          deltaR2 = np.sqrt((jet2.Phi-part.Phi)**2 + (jet2.Eta-part.Eta)**2)
          # If particle in jet 1
          if deltaR1 < R/10:
            index = Vertex_origin(i)
            pt1_part+=part.PT 
            if index == "ISR":
              pt1_ISR+=part.PT
              continue
            if index in Vertex1: continue
            Vertex1.append(index)
            vertex = branchParticle.At(Vertex1[-1])
            
            D1 = branchParticle.At(vertex.D1)
            D2 = branchParticle.At(vertex.D2)
            if np.sqrt((D1.X-vertex.X)**2 + (D1.Y-vertex.Y)**2)<1000:
              pt1_vertex += vertex.PT
            
          if deltaR2 < R/10:
            index = Vertex_origin(i)
            pt2_part+=part.PT
            if index =="ISR":
              pt2_ISR+=part.PT
              continue
            if index in Vertex2: continue
            Vertex2.append(index)
            vertex = branchParticle.At(Vertex2[-1])
            
            D1 = branchParticle.At(vertex.D1)
            D2 = branchParticle.At(vertex.D2)
            if np.sqrt((D1.X-vertex.X)**2 + (D1.Y-vertex.Y)**2)<1000:
              pt2_vertex += vertex.PT

      
      #Fill histograms
      histPT1_jet.Fill(jet1.PT)
      histPT2_jet.Fill(jet2.PT)
      histPT1_vertex.Fill(pt1_vertex)
      histPT2_vertex.Fill(pt2_vertex)
      histPT1_part.Fill(pt1_part)
      histPT2_part.Fill(pt2_part)
      histPT1_ISR.Fill(pt1_ISR)
      histPT2_ISR.Fill(pt2_ISR)
      pt_list_jet.append(jet1.PT)
      pt_list_vertex.append(pt1_vertex)
      pt_list_ISR.append(pt1_ISR)
      pt_list_sum.append(pt1_vertex+pt1_ISR)
      

# Normalize histograms
if histPT1_vertex.GetSumw2N()==0:
  histPT1_vertex.Sumw2(True)
if histPT2_vertex.GetSumw2N()==0:
  histPT2_vertex.Sumw2(True)
if histPT1_part.GetSumw2N()==0:
  histPT1_part.Sumw2(True)
if histPT2_part.GetSumw2N()==0:
  histPT2_part.Sumw2(True)
if histPT1_ISR.GetSumw2N()==0:
  histPT1_ISR.Sumw2(True)
if histPT2_ISR.GetSumw2N()==0:
  histPT2_ISR.Sumw2(True)
if histPT1_jet.GetSumw2N()==0:
  histPT1_jet.Sumw2(True)
if histPT2_jet.GetSumw2N()==0:
  histPT2_jet.Sumw2(True)

histPT1_vertex.Scale(1./histPT1_vertex.Integral())
histPT2_vertex.Scale(1./histPT2_vertex.Integral())
histPT1_part.Scale(1./histPT1_part.Integral())
histPT2_part.Scale(1./histPT2_part.Integral())
histPT1_ISR.Scale(1./histPT1_ISR.Integral())
histPT2_ISR.Scale(1./histPT2_ISR.Integral())
histPT1_jet.Scale(1./histPT1_jet.Integral())
histPT2_jet.Scale(1./histPT2_jet.Integral())

#Plots
c1 = ROOT.TCanvas("c1","PT1",600,600)
c2 = ROOT.TCanvas("c2","PT2",600,600)
tau=10
Nf=2

c1.cd()
leg1 = ROOT.TLegend(0.4,0.8,0.9,0.9)

histPT1_vertex.SetTitle("pT for Jet 1 / c#tau=%i mm / Nf=%i"%(tau,Nf))
histPT1_vertex.SetMaximum(0.5)
histPT1_vertex.SetMinimum(0.0)
histPT1_vertex.SetStats(0)
histPT1_vertex.SetLineColor(1)
histPT1_vertex.Draw("E1X0 HIST")
histPT1_part.SetStats(0)
histPT1_part.SetLineColor(2)
histPT1_part.Draw("Same E1X0 HIST")
histPT1_ISR.SetStats(0)
histPT1_ISR.SetLineColor(3)
histPT1_ISR.Draw("Same E1X0 HIST")
histPT1_jet.SetStats(0)
histPT1_jet.SetLineColor(4)
histPT1_jet.Draw("Same E1X0 HIST")


leg1.AddEntry(histPT1_part,"From particles in jet, Mean = %.2f"%(histPT1_part.GetMean()))
leg1.AddEntry(histPT1_vertex,"From dark rhos, Mean = %.2f"%(histPT1_vertex.GetMean()))
leg1.AddEntry(histPT1_ISR,"From ISR, Mean = %.2f"%(histPT1_ISR.GetMean()))
leg1.AddEntry(histPT1_jet,"From jet, Mean = %.2f"%(histPT1_jet.GetMean()))

leg1.Draw("Same")

c1.Update()
ROOT.gSystem.ProcessEvents()


c2.cd()
leg2 = ROOT.TLegend(0.4,0.8,0.9,0.9)

histPT2_vertex.SetTitle("pT for Jet 2 / c#tau=%i mm / Nf=%i"%(tau,Nf))
histPT2_vertex.SetStats(0)
histPT2_vertex.SetLineColor(1)
histPT2_vertex.Draw("E1X0 HIST")
histPT2_part.SetStats(0)
histPT2_part.SetLineColor(2)
histPT2_part.Draw("Same E1X0 HIST")
histPT2_ISR.SetStats(0)
histPT2_ISR.SetLineColor(3)
histPT2_ISR.Draw("Same E1X0 HIST")
histPT2_jet.SetStats(0)
histPT2_jet.SetLineColor(4)
histPT2_jet.Draw("Same E1X0 HIST")

leg2.AddEntry(histPT2_vertex,"From dark rhos, Mean = %.2f"%(histPT2_vertex.GetMean()))
leg2.AddEntry(histPT2_part,"From particles in jet, Mean = %.2f"%(histPT2_part.GetMean()))
leg2.AddEntry(histPT2_ISR,"From ISR, Mean = %.2f"%(histPT2_ISR.GetMean()))
leg2.AddEntry(histPT2_jet,"From jet, Mean = %.2f"%(histPT2_jet.GetMean()))
leg2.Draw("Same")

c2.Update()
ROOT.gSystem.ProcessEvents()


raw_input("Press enter to close")
