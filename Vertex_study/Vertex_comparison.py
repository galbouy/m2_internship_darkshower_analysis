# Comparison of two lists of histograms comming from Vertex_study_histlists.py

# Input : two histlists.root
# Output : comparative plots for Ntrk/vtx, pT/vtx, Nvtx/jets, 
#                                pT charged particles, mJJ/vtx

# bash commmand : python Vertex_comparison.py histlist1.root histlist2.root

# Author : G. Albouy, LPSC Grenoble, France

import sys
import numpy as np
import ROOT
from array import array

try:
  input = raw_input
except:
  pass

if len(sys.argv) < 3:
  print(" Usage: Vertex_position.py inputFile1 inputFile2")
  sys.exit(1)


inputFile1 = sys.argv[1]
legend1 = "File 1"
inputFile2 = sys.argv[2]
legend2 = "File 2"


print("Input files :")
print(inputFile1)
print(inputFile2)

histlist1 = ROOT.TFile(inputFile1)

histNtrk1_per_vertex1 = ROOT.gROOT.FindObject('ntrk1_per_vertex')
histNtrk2_per_vertex1 = ROOT.gROOT.FindObject('ntrk2_per_vertex')
histPT1_per_vertex1 = ROOT.gROOT.FindObject('pt1_per_vertex')
histNvertex1 = ROOT.gROOT.FindObject('nvtx1')
histPT_charged1 = ROOT.gROOT.FindObject('pt_charge')
histMass_vertex1 = ROOT.gROOT.FindObject('mass_vertex')

print("Acceptance 1 : %.2f %%"%(histNvertex1.GetEntries()/10000 *100))

histlist2 = ROOT.TFile(inputFile2)

histNtrk1_per_vertex2 = ROOT.gROOT.FindObject('ntrk1_per_vertex')
histNtrk2_per_vertex2 = ROOT.gROOT.FindObject('ntrk2_per_vertex')
histPT1_per_vertex2 = ROOT.gROOT.FindObject('pt1_per_vertex')
histNvertex2 = ROOT.gROOT.FindObject('nvtx1')
histPT_charged2 = ROOT.gROOT.FindObject('pt_charge')
histMass_vertex2 = ROOT.gROOT.FindObject('mass_vertex')

print("Acceptance 2 : %.2f %%"%(histNvertex2.GetEntries()/10000 *100))

#histNtrk1_per_vertex1 = histNtrk1_per_vertex1 + histNtrk2_per_vertex1
#histNtrk1_per_vertex2 = histNtrk1_per_vertex2 + histNtrk2_per_vertex2

c1 = ROOT.TCanvas("c1","Ntrk_vertex",600,600)
c2 = ROOT.TCanvas("c2","PT_vertex",600,600)
c3 = ROOT.TCanvas("c3","Nvertex",600,600)
c4 = ROOT.TCanvas("c4","PT_charge",600,600)
c5 = ROOT.TCanvas("c6","Mjj_vertex",600,600)

c1.cd()
leg1 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histNtrk1_per_vertex1.GetBinContent(histNtrk1_per_vertex1.GetMaximumBin()), histNtrk1_per_vertex2.GetBinContent(histNtrk1_per_vertex2.GetMaximumBin())])
histNtrk1_per_vertex1.SetMaximum(maxi)
histNtrk1_per_vertex1.SetMinimum(0.)
histNtrk1_per_vertex1.SetTitle("Ntrk per vertex")
histNtrk1_per_vertex1.SetStats(0)
histNtrk1_per_vertex1.SetLineColor(1)
histNtrk1_per_vertex1.Draw("E1X0 HIST")
histNtrk1_per_vertex2.SetStats(0)
histNtrk1_per_vertex2.SetLineColor(2)
histNtrk1_per_vertex2.Draw("Same E1X0 HIST")

leg1.AddEntry(histNtrk1_per_vertex1, "%s, Mean = %.2f"%(legend1,histNtrk1_per_vertex1.GetMean()))
leg1.AddEntry(histNtrk1_per_vertex2, "%s, Mean = %.2f"%(legend2,histNtrk1_per_vertex2.GetMean()))
leg1.Draw("Same")

c1.Update()
ROOT.gSystem.ProcessEvents()


c2.cd()
leg2 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histPT1_per_vertex1.GetBinContent(histPT1_per_vertex1.GetMaximumBin()), histPT1_per_vertex2.GetBinContent(histPT1_per_vertex2.GetMaximumBin())])
histPT1_per_vertex1.SetMaximum(maxi)
histPT1_per_vertex1.SetMinimum(0.)
histPT1_per_vertex1.SetTitle("p_{T} per vertex")
histPT1_per_vertex1.SetStats(0)
histPT1_per_vertex1.SetLineColor(1)
histPT1_per_vertex1.Draw("E1X0 HIST")
histPT1_per_vertex2.SetStats(0)
histPT1_per_vertex2.SetLineColor(2)
histPT1_per_vertex2.Draw("Same E1X0 HIST")

leg2.AddEntry(histPT1_per_vertex1, "%s, Mean = %.2f"%(legend1,histPT1_per_vertex1.GetMean()))
leg2.AddEntry(histPT1_per_vertex2, "%s, Mean = %.2f"%(legend2,histPT1_per_vertex2.GetMean()))
leg2.Draw("Same")

c2.Update()
ROOT.gSystem.ProcessEvents()


c3.cd()
leg3 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histNvertex1.GetBinContent(histNvertex1.GetMaximumBin()), histNvertex2.GetBinContent(histNvertex2.GetMaximumBin())])
histNvertex1.SetMaximum(maxi)
histNvertex1.SetMinimum(0.)
histNvertex1.SetTitle("Nvertex per jet")
histNvertex1.SetStats(0)
histNvertex1.SetLineColor(1)
histNvertex1.Draw("E1X0 HIST")
histNvertex2.SetStats(0)
histNvertex2.SetLineColor(2)
histNvertex2.Draw("Same E1X0 HIST")

leg3.AddEntry(histNvertex1, "%s, Mean = %.2f"%("Nc=2",histNvertex1.GetMean()))
leg3.AddEntry(histNvertex2, "%s, Mean = %.2f"%("Nc=5",histNvertex2.GetMean()))
leg3.Draw("Same")

c3.Update()
ROOT.gSystem.ProcessEvents()


c4.cd()
leg4 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histPT_charged1.GetBinContent(histPT_charged1.GetMaximumBin()), histPT_charged2.GetBinContent(histPT_charged2.GetMaximumBin())])
histPT_charged1.SetMaximum(maxi)
histPT_charged1.SetMinimum(0.)
histPT_charged1.SetTitle("p_{T} from charged particles")
histPT_charged1.SetStats(0)
histPT_charged1.SetLineColor(1)
histPT_charged1.Draw("E1X0 HIST")
histPT_charged2.SetStats(0)
histPT_charged2.SetLineColor(2)
histPT_charged2.Draw("Same E1X0 HIST")

leg4.AddEntry(histPT_charged1, "%s, Mean = %.2f"%(legend1,histPT_charged1.GetMean()))
leg4.AddEntry(histPT_charged2, "%s, Mean = %.2f"%(legend2,histPT_charged2.GetMean()))
leg4.Draw("Same")

c4.Update()
ROOT.gSystem.ProcessEvents()


c5.cd()
leg5 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histMass_vertex1.GetBinContent(histMass_vertex1.GetMaximumBin()), histMass_vertex2.GetBinContent(histMass_vertex2.GetMaximumBin())])
histMass_vertex1.SetMaximum(maxi)
histMass_vertex1.SetMinimum(0.)
if len(sys.argv[1])>5 or len(sys.argv[2])>5:
  histMass_vertex1.GetXaxis().SetRangeUser(0.,22.)
histMass_vertex1.SetTitle("Vertex invariant mass")
histMass_vertex1.SetStats(0)
histMass_vertex1.SetLineColor(1)
histMass_vertex1.Draw("E1X0 HIST")
histMass_vertex2.SetStats(0)
histMass_vertex2.SetLineColor(2)
histMass_vertex2.Draw("Same E1X0 HIST")

leg5.AddEntry(histMass_vertex1, "%s, Mean = %.2f"%(legend1,histMass_vertex1.GetMean()))
leg5.AddEntry(histMass_vertex2, "%s, Mean = %.2f"%(legend2,histMass_vertex2.GetMean()))
leg5.Draw("Same")

c5.Update()
ROOT.gSystem.ProcessEvents()


raw_input("Press enter to close...")

