# Study of vertices for Delphes file without LRT

# Input : delphes file
# Output : plots of Ntrk/vtx, pT/vtx, Nvtx/jets, 
#                   pT charged particles, pT muons, mJJ/vtx

# bash commmand : python Vertex_study_no_LRT.py delphes_file.root

# Author : G. Albouy, LPSC Grenoble, France

import sys
import numpy as np
import ROOT
from array import array

try:
  input = raw_input
except:
  pass

if len(sys.argv) < 2:
  print(" Usage: Vertex_study_no_LRT.py delphes_file.root")
  sys.exit(1)

ROOT.gSystem.Load("libDelphes")

try:
  ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
  ROOT.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')
except:
  pass

inputFile = sys.argv[1]

print("Input Files :")
print(inputFile)

# Create chain of root trees
chain = ROOT.TChain("Delphes")
chain.Add(inputFile)

# Create object of class ExRootTreeReader
treeReader = ROOT.ExRootTreeReader(chain)
numberOfEntries = treeReader.GetEntries()

print("N events : %.0f"%numberOfEntries)

# Get pointers to branches used in this analysis
R = "14"       # Radius of jets reconstruction
branchJet = treeReader.UseBranch("ParticleFlowJet%s"%R)
branchParticle = treeReader.UseBranch("Particle")
branchEFlowTrack = treeReader.UseBranch("EFlowTrack")
branchEFlowPhoton = treeReader.UseBranch("EFlowPhoton")
branchEFlowNeutralHadron = treeReader.UseBranch("EFlowNeutralHadron")
branchTrack = treeReader.UseBranch("Track")
R = 14.

# Book histograms
histJet1Ntrk = ROOT.TH1F("jet1_ncharge1", "Ncharged Jet 1;Ntrk;A.U.", 15, 0., 120)
histJet2Ntrk = ROOT.TH1F("jet2_ncharge1", "Ncharged Jet 1;Ntrk;A.U.", 15, 0., 120)
histJet1PT = ROOT.TH1F("jet1_pt", "Jet 1 PT;p_{T} (GeV);A.U.",20, 0., 1200)
histJet2PT = ROOT.TH1F("jet2_pt", "Jet 2 PT;p_{T} (GeV);A.U.",20, 0., 1200)

histPT1_vertex = ROOT.TH1F("pt1_vertex", "PT from vertex Jet 1;p_{T} (GeV);Nevents",20, 0., 1200)
histPT2_vertex = ROOT.TH1F("pt2_vertex", "PT from vertex Jet 2;p_{T} (GeV);Nevents",20, 0., 1200)
 

histMass_vertex = ROOT.TH1F("mass_vertex", "m_{JJ} per vertex;m_{JJ} (GeV);Nevents",40, 0., 23)

histNtrk1_vertex = ROOT.TH1F("ncharge1_vertex", "Ncharged from vertex Jet 1;Ncharged;Nevents", 15, 0., 120)
histNtrk2_vertex = ROOT.TH1F("ncharge2_vertex", "Ncharged from vertex Jet 2;N charged;Nevents", 15, 0, 120)

histNvertex1 = ROOT.TH1F("nvtx1", "Nvertex in Jet 1;Nvtx;Nevents",15, 0., 70.)
histNvertex2 = ROOT.TH1F("nvtx2", "Nvertex in Jet 2;Nvtx;Nevents",15, 0., 70.)
histNvertex_uncharge = ROOT.TH1F("nvtx_uncharge", "Proportion of vertices with no tracks;%;Nevents",30, 0., 100.)

histPT1_per_vertex = ROOT.TH1F("pt1_per_vertex", "PT per vertex for Jet 1;p_{T};Nevents", 20, 0., 300.)
histPT2_per_vertex = ROOT.TH1F("pt2_per_vertex", "PT per vertex for Jet 2;p_{T};Nevents", 20, 0., 300.)

histPT_rho = ROOT.TH1F("pt_rho", "PT rho;p_{T};Nevents", 20, 0., 300.)

histPT_charged = ROOT.TH1F("pt_charge", "PT of charged particles;p_{T};Nevents", 30, 0., 100.)
histPT_muon = ROOT.TH1F("pt_muon", "PT of muons;p_{T};Nevents", 30, 0., 200.)

histNtrk1_per_vertex = ROOT.TH1F("ntrk1_per_vertex", "Ntrk per vertex for Jet1;Ntrk;Nevents", 20, 0, 20)
histNtrk2_per_vertex = ROOT.TH1F("ntrk2_per_vertex", "Ntrk per vertex for Jet2;Ntrk;Nevents", 20, 0, 20)

# Function to find decayed stable particles from a vertex
def Vertex(i):
    D = array( 'i' , [i])
    Stable = array( 'i' , [])
    while len(D)!=0:
        D1 = branchParticle.At(D[0]).D1
        D2 = branchParticle.At(D[0]).D2
        for i in range(D1,D2+1):
          if branchParticle.At(i).Status==1:
            if i in Stable: continue
            Stable.append(i)
          else:
            if i in D: continue
            D.append(i)
          
        del D[0]
    return Stable

# Loop over all events
Nevent = 1000
Nevent = numberOfEntries

# setup toolbar
sys.stdout.write("Progression [%s]" % (" " * 50))
sys.stdout.flush()
sys.stdout.write("\b" * (50+1)) # return to start of line, after '['

for entry in range(0,Nevent):
  #if entry%100==0: print(entry)

  # Read event
  treeReader.ReadEntry(entry)
  # Get number of particles and jets
  Npart = branchParticle.GetEntries()
  Njet = branchJet.GetEntries()
  # Set variables and lists
  Ncharge1, Ncharge2 = 0, 0
  PT1_vertex, PT2_vertex = 0.0, 0.0
  Dark_PT1, Dark_PT2 = 0.0, 0.0
  Nvertex = 0.
  Nvertex1_charge, Nvertex2_charge = 0., 0.

  # 1st selection on Njet
  if Njet>1:
    jet1 = branchJet.At(0)
    jet2 = branchJet.At(1)
    # 2nd selection on pT and eta
    if jet1.PT>500. and np.abs(jet1.Eta)<2.5 and jet2.PT>0. and np.abs(jet2.Eta)<2.5:
      #print("Event number : %i"%entry)
      #print("Number of particles : %i"%Npart)
      
      # Loop over all particles
      for i in range(0,Npart):
        # if particle is a rho
        if branchParticle.At(i).PID == 4900113:
          part = branchParticle.At(i)
          D1 = branchParticle.At(part.D1)
          # Compute dR between jets and part
          deltaR1 = np.sqrt((jet1.Phi-part.Phi)**2 + (jet1.Eta-part.Eta)**2)
          deltaR2 = np.sqrt((jet2.Phi-part.Phi)**2 + (jet2.Eta-part.Eta)**2)
          # If particle in jet 1
          if deltaR1 < R/10:
            Nvertex+=1
            Stable_vertex = Vertex(i)
            ntrk = 0
            pT = 0.
            P4_vertex = part.P4()
            for j in Stable_vertex:
              part_vertex = branchParticle.At(j)
              if np.abs(part_vertex.PID)!=13 and np.abs(part_vertex.PID)!=12 and np.abs(part_vertex.PID)!=14 and np.abs(part_vertex.PID)!=16:
                pT+=part_vertex.PT
              elif np.abs(part_vertex.PID)==13 and part_vertex.PT<50:
                pT+=part_vertex.PT
              if np.abs(part_vertex.PID)==13:
                histPT_muon.Fill(part_vertex.PT)
              if part_vertex.Charge!=0:
                histPT_charged.Fill(part_vertex.PT)
                if part_vertex.PT>0.5:
                  Ncharge1+=1
                  ntrk+=1
                  P4_vertex+=part_vertex.P4()
                
            P4_vertex-=part.P4()
            if ntrk>1:
              Nvertex1_charge+=1
              histNtrk1_per_vertex.Fill(ntrk) 
              histPT1_per_vertex.Fill(pT)
              histPT_rho.Fill(part.PT)
              histMass_vertex.Fill(P4_vertex.M())
                
            PT1_vertex+=pT
          
          # If particle in jet 2
          if deltaR2 < R/10:
            Nvertex+=1
            Stable_vertex = Vertex(i)
            ntrk = 0
            pT = 0.
            P4_vertex = part.P4()
            for j in Stable_vertex:
              part_vertex = branchParticle.At(j)
              if np.abs(part_vertex.PID)!=13 and np.abs(part_vertex.PID)!=12 and np.abs(part_vertex.PID)!=14 and np.abs(part_vertex.PID)!=16:
                pT+=part_vertex.PT
              elif np.abs(part_vertex.PID)==13 and part_vertex.PT<50:
                pT+=part_vertex.PT
              if np.abs(part_vertex.PID)==13:
                histPT_muon.Fill(part_vertex.PT)
              if part_vertex.Charge!=0:
                histPT_charged.Fill(part_vertex.PT)
                if part_vertex.PT>0.5:
                  Ncharge2+=1
                  ntrk+=1
                  P4_vertex+=part_vertex.P4()
                
            P4_vertex-=part.P4()
            if ntrk>1:
              Nvertex2_charge+=1
              histNtrk2_per_vertex.Fill(ntrk) 
              histPT2_per_vertex.Fill(pT)
              histMass_vertex.Fill(P4_vertex.M())
              
            PT2_vertex+=pT
          
      # Fill histograms
      histJet1Ntrk.Fill(jet1.NCharged)
      histJet2Ntrk.Fill(jet2.NCharged)
      histNtrk1_vertex.Fill(Ncharge1)
      histNtrk2_vertex.Fill(Ncharge2)

      histNvertex1.Fill(Nvertex1_charge)
      histNvertex2.Fill(Nvertex2_charge)

      if Nvertex!=0: 
        histNvertex_uncharge.Fill((1.-(Nvertex1_charge+Nvertex2_charge)/Nvertex)*100)

      histJet1PT.Fill(jet1.PT)
      histJet2PT.Fill(jet2.PT)
      histPT1_vertex.Fill(PT1_vertex)
      histPT2_vertex.Fill(PT2_vertex)
      
      #raw_input("Enter")
  # update the bar
  if entry%(int(Nevent/50))==0:
    sys.stdout.write("#")
    sys.stdout.flush()

sys.stdout.write("]\n") # this ends the progress bar
print("Acceptance : %.2f %%"%(histJet1PT.GetEntries()/Nevent *100))

# Normalize histograms
if histJet1Ntrk.GetSumw2N()==0:
  histJet1Ntrk.Sumw2(True)
if histJet1PT.GetSumw2N()==0:
  histJet1PT.Sumw2(True)
if histNtrk1_vertex.GetSumw2N()==0:
  histNtrk1_vertex.Sumw2(True)
if histPT1_vertex.GetSumw2N()==0:
  histPT1_vertex.Sumw2(True)
if histJet2Ntrk.GetSumw2N()==0:
  histJet2Ntrk.Sumw2(True)
if histJet2PT.GetSumw2N()==0:
  histJet2PT.Sumw2(True)
if histNtrk2_vertex.GetSumw2N()==0:
  histNtrk2_vertex.Sumw2(True)
if histPT2_vertex.GetSumw2N()==0:
  histPT2_vertex.Sumw2(True)
if histNtrk1_per_vertex.GetSumw2N()==0:
  histNtrk1_per_vertex.Sumw2(True)
if histPT1_per_vertex.GetSumw2N()==0:
  histPT1_per_vertex.Sumw2(True)
if histNtrk2_per_vertex.GetSumw2N()==0:
  histNtrk2_per_vertex.Sumw2(True)
if histPT2_per_vertex.GetSumw2N()==0:
  histPT2_per_vertex.Sumw2(True)
if histNvertex1.GetSumw2N()==0:
  histNvertex1.Sumw2(True)
if histNvertex2.GetSumw2N()==0:
  histNvertex2.Sumw2(True)

if histPT_rho.GetSumw2N()==0:
  histPT_rho.Sumw2(True)
histPT_rho.Scale(1./histPT_rho.Integral())

histJet1Ntrk.Scale(1./histJet1Ntrk.Integral())
histJet1PT.Scale(1./histJet1PT.Integral())
histNtrk1_vertex.Scale(1./histNtrk1_vertex.Integral())
histPT1_vertex.Scale(1./histPT1_vertex.Integral())
histJet2Ntrk.Scale(1./histJet2Ntrk.Integral())
histJet2PT.Scale(1./histJet2PT.Integral())
histNtrk2_vertex.Scale(1./histNtrk2_vertex.Integral())
histPT2_vertex.Scale(1./histPT2_vertex.Integral())
histNtrk1_per_vertex.Scale(1./histNtrk1_per_vertex.Integral())
histNtrk2_per_vertex.Scale(1./histNtrk2_per_vertex.Integral())
histPT1_per_vertex.Scale(1./histPT1_per_vertex.Integral())
histPT2_per_vertex.Scale(1./histPT2_per_vertex.Integral())
histNvertex1.Scale(1./histNvertex1.Integral())
histNvertex2.Scale(1./histNvertex2.Integral())


# Plots
c1 = ROOT.TCanvas("c1","Ntrk1",600,600)
c2 = ROOT.TCanvas("c2","Ntrk2",600,600)
c3 = ROOT.TCanvas("c3","PT1",600,600)
c4 = ROOT.TCanvas("c4","PT2",600,600)
c6 = ROOT.TCanvas("c6","Ntrk/vtx",600,600)
c7 = ROOT.TCanvas("c7","PT/vtx",600,600)
c8 = ROOT.TCanvas("c8","Nvtx",600,600)
c9 = ROOT.TCanvas("c9","Mjj_vertex",600,600)
c10 = ROOT.TCanvas("c10","Vertex_uncharge",600,600)
c11 = ROOT.TCanvas("c11","PT_charged",600,600)
c12 = ROOT.TCanvas("c12","PT_muon",600,600)


c1.cd()
leg1 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histJet1Ntrk.GetBinContent(histJet1Ntrk.GetMaximumBin()), histNtrk1_vertex.GetBinContent(histNtrk1_vertex.GetMaximumBin())])
histJet1Ntrk.SetMaximum(maxi)
histJet1Ntrk.SetMinimum(0.)
histJet1Ntrk.SetTitle("Ntrk for Jet 1")
histJet1Ntrk.SetStats(0)
histJet1Ntrk.SetLineColor(1)
histJet1Ntrk.Draw("E1X0 HIST")
histNtrk1_vertex.SetStats(0)
histNtrk1_vertex.SetLineColor(2)
histNtrk1_vertex.Draw("Same E1X0 HIST")

leg1.AddEntry(histJet1Ntrk, "From jet, Mean = %.2f"%histJet1Ntrk.GetMean())
leg1.AddEntry(histNtrk1_vertex, "From #rho_{d}, Mean = %.2f"%histNtrk1_vertex.GetMean())
leg1.Draw("Same")

c1.Update()
ROOT.gSystem.ProcessEvents()


c2.cd()
leg2 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histJet2Ntrk.GetBinContent(histJet2Ntrk.GetMaximumBin()), histNtrk2_vertex.GetBinContent(histNtrk2_vertex.GetMaximumBin())])
histJet2Ntrk.SetMaximum(maxi)
histJet2Ntrk.SetMinimum(0.)
histJet2Ntrk.SetTitle("Ntrk for Jet 2")
histJet2Ntrk.SetStats(0)
histJet2Ntrk.SetLineColor(1)
histJet2Ntrk.Draw("E1X0 HIST")
histNtrk2_vertex.SetStats(0)
histNtrk2_vertex.SetLineColor(2)
histNtrk2_vertex.Draw("Same E1X0 HIST")


leg2.AddEntry(histJet1Ntrk, "From jet, Mean = %.2f"%histJet2Ntrk.GetMean())
leg2.AddEntry(histNtrk1_vertex, "From #rho_{d}, Mean = %.2f"%histNtrk2_vertex.GetMean())
leg2.Draw("Same")

c2.Update()
ROOT.gSystem.ProcessEvents()

c3.cd()
leg3 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histJet1PT.GetBinContent(histJet1PT.GetMaximumBin()), histPT1_vertex.GetBinContent(histPT1_vertex.GetMaximumBin())])
histJet1PT.SetMaximum(maxi)
histJet1PT.SetMinimum(0.)
histJet1PT.SetTitle("p_{T} for Jet 1")
histJet1PT.SetStats(0)
histJet1PT.SetLineColor(1)
histJet1PT.Draw("E1X0 HIST")
histPT1_vertex.SetStats(0)
histPT1_vertex.SetLineColor(2)
histPT1_vertex.Draw("Same E1X0 HIST")


leg3.AddEntry(histJet1PT, "p_{T} from jet, Mean = %.2f"%histJet1PT.GetMean())
leg3.AddEntry(histPT1_vertex, "p_{T} from #rho_{d}, Mean = %.2f"%histPT1_vertex.GetMean())
leg3.Draw("Same")

c3.Update()
ROOT.gSystem.ProcessEvents()

c4.cd()
leg4 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histJet2PT.GetBinContent(histJet2PT.GetMaximumBin()), histPT2_vertex.GetBinContent(histPT2_vertex.GetMaximumBin())])
histJet2PT.SetMaximum(maxi)
histJet2PT.SetMinimum(0.)
histJet2PT.SetTitle("p_{T} for Jet 2")
histJet2PT.SetStats(0)
histJet2PT.SetLineColor(1)
histJet2PT.Draw("E1X0 HIST")
histPT2_vertex.SetStats(0)
histPT2_vertex.SetLineColor(2)
histPT2_vertex.Draw("Same E1X0 HIST")


leg4.AddEntry(histJet2PT, "p_{T} from jet, Mean = %.2f"%histJet2PT.GetMean())
leg4.AddEntry(histPT2_vertex, "p_{T} from #rho_{d}, Mean = %.2f"%histPT2_vertex.GetMean())
leg4.Draw("Same")

c4.Update()
ROOT.gSystem.ProcessEvents()


c6.cd()
leg6 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histNtrk1_per_vertex.GetBinContent(histNtrk1_per_vertex.GetMaximumBin()), histNtrk2_per_vertex.GetBinContent(histNtrk2_per_vertex.GetMaximumBin())])
histNtrk1_per_vertex.SetMaximum(maxi)
histNtrk1_per_vertex.SetMinimum(0.)
histNtrk1_per_vertex.SetTitle("Ntrk per vertices")
histNtrk1_per_vertex.SetStats(0)
histNtrk1_per_vertex.SetLineColor(1)
histNtrk1_per_vertex.Draw("E1X0HIST")
histNtrk2_per_vertex.SetStats(0)
histNtrk2_per_vertex.SetLineColor(2)
histNtrk2_per_vertex.Draw("Same E1X0HIST")

leg6.AddEntry(histNtrk1_per_vertex, "Jet 1, Mean = %.2f"%histNtrk1_per_vertex.GetMean())
leg6.AddEntry(histNtrk2_per_vertex, "Jet 2, Mean = %.2f"%histNtrk2_per_vertex.GetMean())
leg6.Draw("Same")

c6.Update()
ROOT.gSystem.ProcessEvents()

c7.cd()
leg7 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histPT1_per_vertex.GetBinContent(histPT1_per_vertex.GetMaximumBin()), histPT2_per_vertex.GetBinContent(histPT2_per_vertex.GetMaximumBin())])
histPT1_per_vertex.SetMaximum(maxi)
histPT1_per_vertex.SetMinimum(0.)
histPT1_per_vertex.SetTitle("p_{T} per vertices")
histPT1_per_vertex.SetStats(0)
histPT1_per_vertex.SetLineColor(1)
histPT1_per_vertex.Draw("E1X0HIST")
histPT2_per_vertex.SetStats(0)
histPT2_per_vertex.SetLineColor(2)
histPT2_per_vertex.Draw("Same E1X0HIST")

histPT_rho.SetStats(0)
histPT_rho.SetLineColor(3)
histPT_rho.Draw("Same E1X0HIST")

leg7.AddEntry(histPT1_per_vertex, "Jet 1, Mean = %.2f"%histPT1_per_vertex.GetMean())
leg7.AddEntry(histPT2_per_vertex, "Jet 2, Mean = %.2f"%histPT2_per_vertex.GetMean())
leg7.AddEntry(histPT_rho, "pT from rhos in Jet 1, Mean = %.2f"%histPT_rho.GetMean())
leg7.Draw("Same")

c7.Update()
ROOT.gSystem.ProcessEvents()

c8.cd()
leg8 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([histNvertex1.GetBinContent(histNvertex1.GetMaximumBin()), histNvertex2.GetBinContent(histNvertex2.GetMaximumBin())])
histNvertex1.SetMaximum(maxi)
histNvertex1.SetMinimum(0.)
histNvertex1.SetTitle("Nvertex per jets")
histNvertex1.SetStats(0)
histNvertex1.SetLineColor(1)
histNvertex1.Draw("E1X0HIST")
histNvertex2.SetStats(0)
histNvertex2.SetLineColor(2)
histNvertex2.Draw("Same E1X0HIST")

leg8.AddEntry(histNvertex1, "Jet 1, Mean = %.2f"%histNvertex1.GetMean())
leg8.AddEntry(histNvertex2, "Jet 2, Mean = %.2f"%histNvertex2.GetMean())
leg8.Draw("Same")

c8.Update()
ROOT.gSystem.ProcessEvents()

c9.cd()
histMass_vertex.SetTitle("m_{JJ} per vertex from charged particles")
histMass_vertex.Draw("E1X0 HIST")

c9.Update()
ROOT.gSystem.ProcessEvents()

c10.cd()
histNvertex_uncharge.SetTitle("Proportion of rhos not reconstructed as vertex")
histNvertex_uncharge.Draw("E1X0 HIST")

c10.Update()
ROOT.gSystem.ProcessEvents()

c11.cd()
histPT_charged.SetTitle("PT of charged particles")
histPT_charged.Draw("E1X0 HIST")

c11.Update()
ROOT.gSystem.ProcessEvents()

c12.cd()
histPT_muon.SetTitle("PT of muons")
histPT_muon.Draw("E1X0 HIST")

c12.Update()
ROOT.gSystem.ProcessEvents()



raw_input("Press enter to close ...")
