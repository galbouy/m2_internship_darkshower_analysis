Samples generation :

-> Pythia  : need pythia8245 linked to HepMC2 library
             compile mymain01.cc inside examples directory of pythia (add file name in MakeFile)
             command : ./mymain01 pythia_card.cmnd pythia_events.hep


-> Delphes : need Delphes3 linked to ROOT
             use delphes_card_HLLHC.tcl
             command : ./DelphesHepMC delphes_card_HLLHC.tcl delphes_output.root pythia_sample.hep
