// Events generation based on a pythia card
// Conversion to HepMC  

#include "Pythia8/Pythia.h" // Include Pythia headers.
#include "Pythia8Plugins/HepMC2.h"
#include <unistd.h>
using namespace Pythia8;
int main(int argc, char* argv[]) {
  // Set up generation.
  Pythia pythia;

  //Input parameters
  pythia.readFile(argv[1]);

  //Number of events
  int nEvent = pythia.mode("Main:numberOfEvents");

  // Allow abort of run if many errors.
  int  nAbort  = pythia.mode("Main:timesAllowErrors");
  int  iAbort  = 0;

  // Interface for conversion from Pythia8::Event to HepMC one.
  HepMC::Pythia8ToHepMC ToHepMC;
  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io(argv[2], std::ios::out);

  // Initialize
  pythia.init(); 

  // Generate event(s).
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    // Generate events. Quit if many failures.
    if (!pythia.next()) {
      if (++iAbort < nAbort) continue;
      cout << " Event generation aborted prematurely, owing to error!\n";
      break;
    }
    
    // Construct new empty HepMC event.
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    // Fill HepMC event
    ToHepMC.fill_next_event( pythia, hepmcevt );

    // Write the HepMC event to file. Done with it.
    ascii_io << hepmcevt;
    delete hepmcevt;
  }
  return 0;
}
