# Automation of Jet_analysis.py for a directory containing multiple files to analyse
# Necessary to be in Delphes directory
# Input : path to directory
# Output : series of bash commands running Jet_analysis.py for files in directory

# bash command : python Analysis_code/Macro_analysis.py /path/to/directory/

import os
import sys
from array import array

try:
	input = raw_input
except:
	pass

if len(sys.argv) < 2:
	print(" Usage: Analysis_code/Macro_analysis.py /path/")
	sys.exit(1)

path = sys.argv[1]
if path[-1]!="/":
	print(" Usage: Analysis_code/Macro_analysis.py /path/")
	print(" Miss '/' at end of path")
	sys.exit(1)

# Find every files in directory
files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path,f))]


for f in files:
	# Selection of files to analyse / possible to add other conditions
	if f[-13:]=="histlist.root": continue   # skip files ending with histlist.root
	#if f[]=="": continue

	# If Analysis.py not in Delphes directory : need to add path to it
	os.system('python Analysis_code/Jet_analysis.py %s'%(path + f))

print("Done")
