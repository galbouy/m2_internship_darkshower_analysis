# Python analysis code for Delphes simulation files
# Uses PyROOT analysis library and Delphes ExRootTreeReader module
# Necessary to be located in Delphes directory to run 

# Creates a list of histograms of pT, Ntrk, mJJ, MET for the two leading jets with radius R
# Input :  input file with path 
# Output : ROOT file with a list of histograms save in the same location

# bash command : python Analysis_code/Jet_analysis.py /path/to/delphes/file/delphes_file.root

# Author : G. Albouy, LPSC Grenoble, France

import sys
import numpy as np
import ROOT
from array import array

try:
  input = raw_input
except:
  pass

if len(sys.argv) < 2:
  print(" Usage: Analysis_code/Jet_analysis.py /path/delphes_file.root")
  sys.exit(1)

ROOT.gSystem.Load("libDelphes")

try:
	ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
	ROOT.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')
except:
	pass

# Parameters
############################################
# Radius of jets (0.4, 0.8, 1.0, 1.2, 1.4) :
R = 1.4 
# Events selection (pT in GeV)
pT_min_jet1 = 500
pT_min_jet2 = 0
eta_max = 2.5
############################################

inputFile = sys.argv[1]
print("Input file :")
print(inputFile)

# Create chain of root trees
chain = ROOT.TChain("Delphes")
chain.Add(inputFile)

# Create object of class ExRootTreeReader
treeReader = ROOT.ExRootTreeReader(chain)
numberOfEntries = treeReader.GetEntries()

# Get pointer to branches used in this analysis
# R-jet branches : 04, 08, 10, 12, 14
R_jet = str(int(R*10))
if R<1.0:
	R_jet = '0' + R_jet

branchJet = treeReader.UseBranch("ParticleFlowJet%s"%R_jet)
branchMET = treeReader.UseBranch("MissingET")


# Book histograms
Nbins = 30
histJet1PT = ROOT.TH1F("jet1_pt", "Lead jet p_{T} with R=%.1f;p_{T} GeV;A.U."%(R), Nbins, 0.0, 2000.0)
histJet2PT = ROOT.TH1F("jet2_pt", "Sub jet p_{T} with R=%.1f;p_{T} GeV;A.U."%(R), Nbins, 0.0, 2000.0)

histJetmJJ = ROOT.TH1F("jet_mJJ", " Invariant mass m_{JJ} with R=%.1f;m_{JJ} GeV;A.U."%(R), Nbins, 0.0, 3500.0)

Nbins = 20
histJet1Ntrk = ROOT.TH1F("jet1_ntrk", "Lead jet N_{trk} with R=%.1f;N_{trk};A.U."%(R), Nbins, .0, 170.0)
histJet2Ntrk = ROOT.TH1F("jet2_ntrk", "Sub jet N_{trk} with R=%.1f;N_{trk};A.U."%(R), Nbins, .0, 170.0)

histMET = ROOT.TH1F("jet_met", "Missing transverse energy;MET GeV;A.U.", Nbins, .0, 800.0)


# Loop over all events
for entry in range(0, numberOfEntries):
	# Load selected branches with data from specified event
	treeReader.ReadEntry(entry)

	# If event contains at least 2 jet
	if branchJet.GetEntries() > 1:
		# Take the two leading jets
		jet1 = branchJet.At(0)
		jet2 = branchJet.At(1)
		# Events selection
		if jet1.PT > pT_min_jet1 and np.abs(jet1.Eta) < eta_max and jet2.PT > pT_min_jet2 and np.abs(jet2.Eta) < eta_max :
			# Plot Ntrk, pT, mJJ and MET
			histJet1PT.Fill(jet1.PT)
			histJet2PT.Fill(jet2.PT)
			histJetmJJ.Fill((jet1.P4() + jet2.P4()).M())
			histJet1Ntrk.Fill(jet1.NCharged)
			histJet2Ntrk.Fill(jet2.NCharged)
			histMET.Fill(branchMET.At(0).MET)



# Normalize histograms
if histJet1PT.GetSumw2N()==0:
	histJet1PT.Sumw2(True)
if histJet2PT.GetSumw2N()==0:
	histJet2PT.Sumw2(True)
if histJetmJJ.GetSumw2N()==0:
	histJetmJJ.Sumw2(True)
if histJet1Ntrk.GetSumw2N()==0:
	histJet1Ntrk.Sumw2(True)
if histJet2Ntrk.GetSumw2N()==0:
	histJet2Ntrk.Sumw2(True)
if histMET.GetSumw2N()==0:
	histMET.Sumw2(True)

histJet1PT.Scale(1./histJet1PT.Integral())
histJet2PT.Scale(1./histJet2PT.Integral())
histJetmJJ.Scale(1./histJetmJJ.Integral())
histJet1Ntrk.Scale(1./histJet1Ntrk.Integral())
histJet2Ntrk.Scale(1./histJet2Ntrk.Integral())

histMET.Scale(1./histMET.Integral())

# Save histograms to list
histlist = ROOT.TList()

histlist.Add(histJet1PT)
histlist.Add(histJet2PT)
histlist.Add(histJetmJJ)
histlist.Add(histJet1Ntrk)
histlist.Add(histJet2Ntrk)
histlist.Add(histMET)

# Save list to ROOT file with location and filename : 
# /path/to/delphes/file/delphes_file_histlist.root
outputFile = inputFile[:-5] + "_histlist.root"
rootFile = ROOT.TFile(outputFile, "RECREATE")
histlist.Write()
rootFile.Close()






