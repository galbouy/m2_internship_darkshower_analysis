- Python analysis codes for Delphes simulation files
- Need PyROOT analysis library and Delphes
- Necessary to be located in Delphes directory to run 

Jet_analysis.py 
	-> Creates a list of histograms of pT, Ntrk, mJJ, MET for the two leading jets with radius R

	-> bash command : python Analysis_code/Jet_analysis.py /path/to/delphes/file/delphes_file.root


Comparative.py 
	-> Constructs a series of comparative plots based on two lists of histograms produced with Jet_analysis.py

	-> bash command : python Analysis_code/Comparative.py path1/hislist1.root path2/histlist2.root

Macro_analysis.py 
	-> Automation of Jet_analysis.py for a directory containing multiple files to analyse

	-> bash command : python Analysis_code/Macro_analysis.py /path/to/directory/

Reconstruction.py
	-> plot of mean m_JJ for different R-jet reconstruction 
	-> bash command = python Reconstruction.py path/to/delphes/file/delphes_file.root
