# Python analysis code for Delphes simulation files
# Uses PyROOT analysis library and Delphes ExRootTreeReader module
# Necessary to be located in Delphes directory to run 

# Plot mean m_JJ for different R-jet reconstruction
# Input :  input file with path 
# Output : plot m_JJ(R)

# bash command : python Analysis_code/Jet_analysis.py /path/to/delphes/file/delphes_file.root

# Author : G. Albouy, LPSC Grenoble, France
import sys
import numpy as np
import ROOT
from array import array



ROOT.gSystem.Load("libDelphes")

try:
  ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
  ROOT.gInterpreter.Declare('#include "external/ExRootAnalysis/ExRootTreeReader.h"')
except:
  pass


inputFile1 = sys.argv[1]

def GetMean(inputFile):
	# Create chain of root trees
	chain = ROOT.TChain("Delphes")
	chain.Add(inputFile)

	# Create object of class ExRootTreeReader
	treeReader = ROOT.ExRootTreeReader(chain)
	numberOfEntries = treeReader.GetEntries()

	# Get pointers to branches used in this analysis
	branchJet04 = treeReader.UseBranch("ParticleFlowJet04")
	branchJet10 = treeReader.UseBranch("ParticleFlowJet10")
	branchJet14 = treeReader.UseBranch("ParticleFlowJet14")

	# Book histograms
	histJet04Mass = array('d')
	histJet10Mass = array('d')
	histJet14Mass = array('d')


	# Loop over all events
	for entry in range(0, numberOfEntries):
	  # Load selected branches with data from specified event
	  treeReader.ReadEntry(entry)

	  if branchJet04.GetEntries() > 1:
	    # Take first jet
	    jet1 = branchJet04.At(0)
	    # Take second jet
	    jet2 = branchJet04.At(1)

	    if jet1.PT > 500 and jet2.PT > 500 and np.abs(jet1.Eta) < 2.5 and np.abs(jet2.Eta) < 2.5:
	      # Plot invariant mass of the two first jets
	      histJet04Mass.append((jet1.P4() + jet2.P4()).M())

	  if branchJet10.GetEntries() > 1:
	    # Take first jet
	    jet1 = branchJet10.At(0)
	    # Take second jet
	    jet2 = branchJet10.At(1)

	    if jet1.PT > 500 and jet2.PT > 500 and np.abs(jet1.Eta) < 2.5 and np.abs(jet2.Eta) < 2.5:
	      # Plot invariant mass of the two first jets
	      histJet10Mass.append((jet1.P4() + jet2.P4()).M())

	  if branchJet14.GetEntries() > 1:
	    # Take first jet
	    jet1 = branchJet14.At(0)
	    # Take second jet
	    jet2 = branchJet14.At(1)

	    if jet1.PT > 500 and jet2.PT > 500 and np.abs(jet1.Eta) < 2.5 and np.abs(jet2.Eta) < 2.5:
	      # Plot invariant mass of the two first jets
	      histJet14Mass.append((jet1.P4() + jet2.P4()).M())


	moyenne = array('f', [np.mean(histJet04Mass), np.mean(histJet08Mass), np.mean(histJet10Mass), np.mean(histJet12Mass), np.mean(histJet14Mass)])
	return moyenne


moyenne1 = GetMean(inputFile1)

R = array('f', [0.4, 1.0, 1.4])

c1 = ROOT.TCanvas("c1", "mJJ", 600, 600)

c1.cd()
graph1 = ROOT.TGraph(3, R, moyenne1)
graph1.SetTitle("Mean m_{JJ} for different R;R;m_{JJ} (GeV)")
graph1.SetFillColor(0)
graph1.SetLineColor(1)
graph1.SetMarkerColor(1)
graph1.SetMarkerStyle(21)
graph1.Draw("ALP")

c1.Update()
ROOT.gSystem.ProcessEvents()

raw_input("Press enter ton continue ...")



















