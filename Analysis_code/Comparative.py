# Python analysis code for Delphes simulation files
# Using PyROOT analysis library 

# Constructs a series of comparative plots based on two lists of histograms 
# produced with Jet_analysis.py
# Input :  paths and file names of the two lists
# Output : comparative plots

# bash command : python Analysis_code/Comparative.py path1/hislist1.root path2/histlist2.root

# Author : G. Albouy, LPSC Grenoble, France

import sys
import numpy as np
import ROOT
from array import array

try:
  input = raw_input
except:
  pass

if len(sys.argv) < 3:
  print(" Usage: Analysis_code/Comparative.py histlist1.root histlist2.root")
  sys.exit(1)


# Set legends for plots
legend1 = "File 1"
legend2 = "File 2"

inputFile1 = sys.argv[1]
inputFile2 = sys.argv[2]
print("Input files :")
print(inputFile1)
print(inputFile2)

# Fetch histograms from histlist1
histlist1 = ROOT.TFile(inputFile1)

hist1Jet1PT = ROOT.gROOT.FindObject('jet1_pt')
hist1Jet2PT = ROOT.gROOT.FindObject('jet2_pt')
hist1JetmJJ = ROOT.gROOT.FindObject('jet_mJJ')
hist1Jet1Ntrk = ROOT.gROOT.FindObject('jet1_ntrk')
hist1Jet2Ntrk = ROOT.gROOT.FindObject('jet2_ntrk')
hist1MET = ROOT.gROOT.FindObject('jet_met')

# Fetch histograms from histlist2
histlist2 = ROOT.TFile(inputFile2)

hist2Jet1PT = ROOT.gROOT.FindObject('jet1_pt')
hist2Jet2PT = ROOT.gROOT.FindObject('jet2_pt')
hist2JetmJJ = ROOT.gROOT.FindObject('jet_mJJ')
hist2Jet1Ntrk = ROOT.gROOT.FindObject('jet1_ntrk')
hist2Jet2Ntrk = ROOT.gROOT.FindObject('jet2_ntrk')
hist2MET = ROOT.gROOT.FindObject('jet_met')


#  Create canvas and plots histograms
c1 = ROOT.TCanvas("c1","pT1",600,600)
c2 = ROOT.TCanvas("c2","pT2",600,600)
c3 = ROOT.TCanvas("c3","mJJ",600,600)
c4 = ROOT.TCanvas("c4","Ntrk1",600,600)
c5 = ROOT.TCanvas("c5","Ntrk2",600,600)
c6 = ROOT.TCanvas("c6","MET",600,600)

c1.cd()
leg1 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([hist1Jet1PT.GetBinContent(hist1Jet1PT.GetMaximumBin()), hist2Jet1PT.GetBinContent(hist2Jet1PT.GetMaximumBin())])
hist1Jet1PT.SetMaximum(maxi)
hist1Jet1PT.SetMinimum(0.)
hist1Jet1PT.SetTitle("Lead jet p_{T}")
hist1Jet1PT.SetStats(0)
hist1Jet1PT.SetLineColor(1)
hist1Jet1PT.Draw("E1X0 HIST")
hist2Jet1PT.SetStats(0)
hist2Jet1PT.SetLineColor(2)
hist2Jet1PT.Draw("Same E1X0 HIST")

leg1.AddEntry(hist1Jet1PT, "%s, Mean = %.2f"%(legend1,hist1Jet1PT.GetMean()))
leg1.AddEntry(hist2Jet1PT, "%s, Mean = %.2f"%(legend2,hist2Jet1PT.GetMean()))
leg1.Draw("Same")
text1 = ROOT.TLatex(1400., .8*maxi, "#Delta <p_{T}> = %.1f"%((hist2Jet1PT.GetMean()-hist1Jet1PT.GetMean())/hist1Jet1PT.GetMean()*100) + u'%')
text1.SetTextSize(0.03)
text1.Draw("Same")

c1.Update()
ROOT.gSystem.ProcessEvents()


c2.cd()
leg2 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([hist1Jet2PT.GetBinContent(hist1Jet2PT.GetMaximumBin()), hist2Jet2PT.GetBinContent(hist2Jet2PT.GetMaximumBin())])
hist1Jet2PT.SetMaximum(maxi)
hist1Jet2PT.SetMinimum(0.)
hist1Jet2PT.SetTitle("Sub jet p_{T}")
hist1Jet2PT.SetStats(0)
hist1Jet2PT.SetLineColor(1)
hist1Jet2PT.Draw("E1X0 HIST")
hist2Jet2PT.SetStats(0)
hist2Jet2PT.SetLineColor(2)
hist2Jet2PT.Draw("Same E1X0 HIST")

leg2.AddEntry(hist1Jet2PT, "%s, Mean = %.2f"%(legend1,hist1Jet2PT.GetMean()))
leg2.AddEntry(hist2Jet2PT, "%s, Mean = %.2f"%(legend2,hist2Jet2PT.GetMean()))
leg2.Draw("Same")
text2 = ROOT.TLatex(1400., .8*maxi, "#Delta <p_{T}> = %.1f"%((hist2Jet2PT.GetMean()-hist1Jet2PT.GetMean())/hist1Jet2PT.GetMean()*100) + u'%')
text2.SetTextSize(0.03)
text2.Draw("Same")

c2.Update()
ROOT.gSystem.ProcessEvents()


c3.cd()
leg3 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([hist1JetmJJ.GetBinContent(hist1JetmJJ.GetMaximumBin()), hist2JetmJJ.GetBinContent(hist2JetmJJ.GetMaximumBin())])
hist1JetmJJ.SetMaximum(maxi)
hist1JetmJJ.SetMinimum(0.)
hist1JetmJJ.SetTitle("m_{JJ}")
hist1JetmJJ.SetStats(0)
hist1JetmJJ.SetLineColor(1)
hist1JetmJJ.Draw("E1X0 HIST")
hist2JetmJJ.SetStats(0)
hist2JetmJJ.SetLineColor(2)
hist2JetmJJ.Draw("Same E1X0 HIST")

leg3.AddEntry(hist1JetmJJ, "%s, Mean = %.2f"%(legend1,hist1JetmJJ.GetMean()))
leg3.AddEntry(hist2JetmJJ, "%s, Mean = %.2f"%(legend2,hist2JetmJJ.GetMean()))
leg3.Draw("Same")
text1 = ROOT.TLatex(2500., .8*maxi, "#Delta <m_{JJ}> = %.1f"%((hist2JetmJJ.GetMean()-hist1JetmJJ.GetMean())/hist1JetmJJ.GetMean()*100) + u'%')
text1.SetTextSize(0.03)
text1.Draw("Same")

c3.Update()
ROOT.gSystem.ProcessEvents()


c4.cd()
leg4 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([hist1Jet1Ntrk.GetBinContent(hist1Jet1Ntrk.GetMaximumBin()), hist2Jet1Ntrk.GetBinContent(hist2Jet1Ntrk.GetMaximumBin())])
hist1Jet1Ntrk.SetMaximum(maxi)
hist1Jet1Ntrk.SetMinimum(0.)
hist1Jet1Ntrk.SetTitle("Lead jet N_{trk}")
hist1Jet1Ntrk.SetStats(0)
hist1Jet1Ntrk.SetLineColor(1)
hist1Jet1Ntrk.Draw("E1X0 HIST")
hist2Jet1Ntrk.SetStats(0)
hist2Jet1Ntrk.SetLineColor(2)
hist2Jet1Ntrk.Draw("Same E1X0 HIST")

leg4.AddEntry(hist1Jet1Ntrk, "%s, Mean = %.2f"%(legend1,hist1Jet1Ntrk.GetMean()))
leg4.AddEntry(hist2Jet1Ntrk, "%s, Mean = %.2f"%(legend2,hist2Jet1Ntrk.GetMean()))
leg4.Draw("Same")
text4 = ROOT.TLatex(110., .8*maxi, "#Delta <N_{trk}> = %.1f"%((hist2Jet1Ntrk.GetMean()-hist1Jet1Ntrk.GetMean())/hist1Jet1Ntrk.GetMean()*100) + u'%')
text4.SetTextSize(0.03)
text4.Draw("Same")

c4.Update()
ROOT.gSystem.ProcessEvents()


c5.cd()
leg5 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([hist1Jet2Ntrk.GetBinContent(hist1Jet2Ntrk.GetMaximumBin()), hist2Jet2Ntrk.GetBinContent(hist2Jet2Ntrk.GetMaximumBin())])
hist1Jet2Ntrk.SetMaximum(maxi)
hist1Jet2Ntrk.SetMinimum(0.)
hist1Jet2Ntrk.SetTitle("Sub jet N_{trk}")
hist1Jet2Ntrk.SetStats(0)
hist1Jet2Ntrk.SetLineColor(1)
hist1Jet2Ntrk.Draw("E1X0 HIST")
hist2Jet2Ntrk.SetStats(0)
hist2Jet2Ntrk.SetLineColor(2)
hist2Jet2Ntrk.Draw("Same E1X0 HIST")

leg5.AddEntry(hist1Jet2Ntrk, "%s, Mean = %.2f"%(legend1,hist1Jet2Ntrk.GetMean()))
leg5.AddEntry(hist2Jet2Ntrk, "%s, Mean = %.2f"%(legend2,hist2Jet2Ntrk.GetMean()))
leg5.Draw("Same")
text5 = ROOT.TLatex(110., .8*maxi, "#Delta <N_{trk}> = %.1f"%((hist2Jet2Ntrk.GetMean()-hist1Jet2Ntrk.GetMean())/hist1Jet2Ntrk.GetMean()*100) + u'%')
text5.SetTextSize(0.03)
text5.Draw("Same")

c5.Update()
ROOT.gSystem.ProcessEvents()


c6.cd()
leg6 = ROOT.TLegend(0.4,0.8,0.9,0.9)
maxi = 1.2*np.max([hist1MET.GetBinContent(hist1MET.GetMaximumBin()), hist2MET.GetBinContent(hist2MET.GetMaximumBin())])
hist1MET.SetMaximum(maxi)
hist1MET.SetMinimum(0.)
hist1MET.SetTitle("Missing transverse energy")
hist1MET.SetStats(0)
hist1MET.SetLineColor(1)
hist1MET.Draw("E1X0 HIST")
hist2MET.SetStats(0)
hist2MET.SetLineColor(2)
hist2MET.Draw("Same E1X0 HIST")

leg6.AddEntry(hist1MET, "%s, Mean = %.2f"%(legend1,hist1MET.GetMean()))
leg6.AddEntry(hist2MET, "%s, Mean = %.2f"%(legend2,hist2MET.GetMean()))
leg6.Draw("Same")
text6 = ROOT.TLatex(500., .8*maxi, "#Delta <MET> = %.1f"%((hist2MET.GetMean()-hist1MET.GetMean())/hist1MET.GetMean()*100) + u'%')
text6.SetTextSize(0.03)
text6.Draw("Same")

c6.Update()
ROOT.gSystem.ProcessEvents()

raw_input("Press enter to close...")
